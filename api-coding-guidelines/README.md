## CODING STYLE GUIDE

Reference(s):

* [Twitter's Java Style Guide](https://github.com/twitter/commons/blob/master/src/java/com/twitter/common/styleguide.md)

Style Guide:

* Tab size must be set to 4.

* Indent must be set to 4.  
  
* Continuation indent must be set to 8.  

* Use braces with if-else blocks even if they contain a single line of code.

```java
if (x < 0) {
    x = 1;
}
```

* A single line of code must not contain more than 80-120 characters.

* Method declaration:

```java
public String downloadAnInternet(
    Internet internet,
    Tubes tubes,
    Blogosphere blogs,
    Amount<Long, Data> bandwidth
) {
    tubes.download(internet);
    ...
}
```

* Chained method calls:

```java
// - Method calls are isolated to a line.
// - The proper location for a new method call is unambiguous.
// - Stack trace will provide the exact line where error occurred.

Iterable<Module> modules = ImmutableList.<Module>builder()
        .add(new LifecycleModule())
        .add(new AppLauncherModule())
        .addAll(application.getModules())
        .build();
```

* CamelCase for types, camelCase for variables, UPPER_SNAKE for constants.

* Provide documentation. Documentation for a class may range from a single sentence to paragraphs with code examples. Documentation should serve to disambiguate any conceptual blanks in the API, and make it easier to quickly and correctly use your API.

* Add TODO comment for tasks to do in the future.

* Avoid NULL. Mark the function as `@Nullable` if it may return a null value.

* Use interfaces. Interfaces decouple functionality from implementation, allowing you to use multiple implementations without changing consumers.

* Precondition checks are a good practice, since they serve as a well-defined barrier against bad input from callers. As a convention, object parameters to public constructors and methods should always be checked against null, unless null is explicitly allowed.

* Mutable objects carry a burden - you need to make sure that those who are able to mutate it are not violating expectations of other users of the object, and that it's even save for them to modify.
