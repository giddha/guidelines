## DEVELOPMENT PRACTICES

1. [REST API designing guidelines](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9)

2. [HTTP Status Codes](http://www.restapitutorial.com/httpstatuscodes.html)

3. Exception Handling:
    * Only throw exception in exceptional cases.** If something can be handled, then handle it. Exceptional case can be defined as a condition which cannot be handled by: our code (divide by zero), database (violating a unique constraint), etc.
    * Before throwing an exception always try to think of an alternative.** Can the same message be conveyed without throwing an error? For example: If data for a particular date period is not found, instead of throwing an error mentioning that the data is not found for the given date period, we can return a empty list to the user which conveys the same message.
    * Keep the error messages as descriptive as possible.** Try to guide the user to a successful response or let the user know the exact mistake he / she is making.
  
4. Handling incoming requests:
    * The request body must be completely validated before proceeding to operate on the given data.  
    * All mandatory request parameters must be validated.
    * If a parameter has certain constraints (value less than 100, only 15 characters), it must be validated properly.
    * All request parameters must be returned in the response of the request. This is needed because if any parameter is not mandatory, and internally we take some default values, the user must be informed about these values.
    * String inputs must be properly validated for special characters, encoding, etc.
    * All create and update requests must return the created / updated object.
  
5. Returning response:
    * If the response is a list, then it must be paginated. 
  
6. Comments / Documentation
    * If any line or piece of code may confuse (even in the slightest) any developer (self or other) then it must be commented as to why it is being done.
    * Every function must have a few lines of documentation explaining its functionality.
  
7. Database:
    * The schema of a relational database must not be regularly changed. When working on a functionality where the data structure is not clear and the data may be frequently deleted, consider storing such data in a NoSQL database or cache.
    * Database tables must not be altered manually by any developer. A migration must be written to make changes to any particular table.
    * Try to keep migration in a single commit and not with other code. Once this commit is accepted and the migration applied to an environment it will not be reverted back.
  
8. General points:
    * Avoid multiple queries or inner queries made by ORM to get data from the database when it could have been fetched in a single query.
    * A function must ideally do a single task.
    * Avoid null. Use `Optional<T>` in Java instead.