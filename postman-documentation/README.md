## Postman Documentation

This is a guide to adding documentation on Postman. 

Whenever adding documentation on Postman, we should make sure to follow the following points:

- Add details about what the API does.
- Inform about mandatory fields in the documentation.
- Add the entire request body with all the parameters.
- Add examples for success as well as errors.
- Verify before submitting documentation.

**Example**: We will take an example of **Create Stock Group API** to show how we can create documentation on Postman.

### Add details about what the API does.

Add description of the API. Which permission should the user have?

```text
The API is used to create... The user must have "permission" on "scope" to use this API.
```

### Inform about mandatory fields in the documentation.

Add mandatory and optional request and url parameters.

```text
For this, we can provide the following fields:

- **field1** (*mandatory*) description.
- **field2** (*optional*) description. 
- ...
```

### Add the entire request body with all the parameters.

Add the complete JSON request.

```json
{
    "name": "Sub Group",
    "uniqueName": "sg123",
    "hsnNumber": null,
    "sacNumber": null,
    "parentStockGroupUniqueName": "maingroup"
}
```

### Add examples for success as well as errors.

Use the [Specifying Examples](https://learning.postman.com/docs/sending-requests/examples/) feature to add examples for an API request. This will help developers to integrate our APIs seamlessly.

### Verify before submitting documentation.

Test everything before submitting the documentation.

- `{{authKey}}` and other placeholders for sensitive data.
- Request method, URL parameters, Request body.
- Examples.
- The documentation itself.

*Get it reviewed before submission*.

## And we're done.

![Alt text](../assets/api-documentation.png)
