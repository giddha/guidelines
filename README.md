## Guidelines

### API Development

- [API Contributing Guidelines](api-contributing-guidelines)
- [API Coding Guidelines](api-coding-guidelines)
- [API Development Practices](api-development-practices)
- [API Documentation using Postman](postman-documentation)
