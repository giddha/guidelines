# Contributing Guidelines

1. There will be 3 main branches in every project:
	* main (production environment)
	* release (release environment)
	* test (test environment)

2. Whenever a new feature is to be added, the branch must checkout from **main** branch.

```
# stay on the main branch
git checkout -b feature/x 
```

3. Don't commit anything directly on the feature branch.

4. From the feature branch checkout task branches for each individual task.

```
# from feature branch
git checkout -b task/x
```

5. Branch name must be the card number of the feature or task.

```
git checkout -b clickup-34353
git checkout -b GD-123
```

6. The task branch should be merged in the feature branch and the feature branch into test (for testing).
	* Raise PR for review from the task branch to the feature branch.
	* Once approved and merged, raise another PR from the feature branch to the test branch.
	
7. Don't make any new changes when PR is under review.

8. When the feature is ready for production deployment, raise PR from the feature branch (which has all the commits for each individual task) to the main branch.

9. In the Pull Request do the following:
	* Mention the card number in the description.
	* All methods and ambiguous statements must have a comment written. 
	* For bug PRs mention the issue and how you resolve it.
	* Always select option to delete branch on merge.
	
10. Coding guidelines
	* Database changes must be done through migration files. All keywords must be uppercase.
	* Formatting - 4 spaces/1 indentation
	* Test query performance stats for performance related tasks.
	* Add indexes in migration for SQL or NoSQL.
	* Function names whether in Java or SQL must be details and clearly understandable.

11. Use single branch for single task. One branch should not have code for multiple or different tasks.
	* Refactoring should be separate from the task.
	* Java code must be separate from the database changes.
